
export function getProductDatabase() {
    return JSON.parse(localStorage.getItem('productDatabase'));
}

export function findProduct(product_code) {
    
    const productDatabase = getProductDatabase();

    /*
     * Array.find recherche le premier produit correspondant au code de produit spécifié.
     * Si aucun produit n'est trouvé, renvoie false (via l'opérateur null coalescing).
     *
     * https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/find
     * https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator
     */
    return productDatabase.find((product) => product.product_code === product_code) ?? false;
}


export function findVoucher(voucherCode) {

    const voucherDatabase = JSON.parse(localStorage.getItem('voucherDatabase'));
    return voucherDatabase.find((voucher) => voucher.name === voucherCode) ?? false;
}