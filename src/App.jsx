import React from 'react';

// Composants de React Router
import { BrowserRouter, Route, Switch } from 'react-router-dom';

// Barre de navigation
import { NavBar }  from './components/NavBar';

// Pages de l'application
import { Basket }  from './pages/Basket';
import { Home }    from './pages/Home';
import { Product } from './pages/Product';

// Contexte de l'application
import { AppContext } from './AppContext';


export class App extends React.Component {

    
    /*
     * Le state global de l'application, manipulé en utilisant un contexte React.
     *
     * En permettant aux composants enfants dans la hiérarchie d'agir sur ce state au travers du contexte
     * cela permet de profiter du fonctionnement naturel de React de rafraichissement automatique de
     * l'affichage - appel de la méthode render() - après avoir mis à jour le state via setState().
     */
    state = {
        productDatabase: [],
        basket: [],

        voucherRate: 0,

        addToBasket: (product_code) => {

            // Duplication du state contenant le panier.
            let basket = [ ...this.state.basket ];

            // Recherche du produit dans le panier.
            let basketItem = basket.find((basketItem) => basketItem.product_code === product_code);

            // Est-ce que le produit existe déjà dans le panier ?
            if(basketItem === undefined) {
                // Non, ajout initial du produit.
                basket.push({ product_code: product_code, quantity: 1 });
            } else {
                // Oui, mise à jour de la quantité du produit.
                basketItem.quantity++;
            }

            // Mise à jour du panier.
            this.setState({ basket: basket });
        },

        clearBasket: () => {

            // Vidage complet du panier.
            this.setState({ basket: [] });
        },
        setVoucherRate: (newVoucherRate) => {
            this.setState({voucherRate : newVoucherRate});
          } //appliquer un taux de réduction
    };


    componentDidMount() {

        // const productDatabase = [

        //     { product_code: 'DBZ', description: 'Dragonball Z Kai - Saga de Boo',   price: 29.90 },
        //     { product_code: 'FMA', description: 'Full Metal Alchemist Brotherhood', price: 19.50 },
        //     { product_code: 'SKY', description: 'Skyfall',                          price: 22.50 },
        //     { product_code: 'OPM', description: 'One Punch Man',                    price: 25.70 },
        //     { product_code: 'SWT', description: 'Star Wars épisode V',              price: 29.90 }
        // ];

        const urlHeroku = 'https://geek-shop-back-heroku.herokuapp.com';

        const productsPromise = fetch(`${urlHeroku}/products`);

        productsPromise
        .then(res => res.json())
        .then(result => {
            this.setState({productDatabase : result });
            localStorage.setItem('productDatabase', JSON.stringify(result));
        })

        // const voucherDatabase = [

        //     { name: 'NOEL2020',     reduction    : 0.12 },
        //     { name: 'NOEL2021',     reduction    : 0.50 },
        //     { name: 'ANNIVERSAIRE', reduction    : 0.15 },
        //     { name: 'SOLDES_ETE',   reduction    : 0.25 }
        // ];

        const voucherPromise = fetch(`${urlHeroku}/vouchers`);

        voucherPromise
        .then(res => res.json())
        .then(result => {
            console.log(result);
            this.setState({voucherDatabase : result });
            localStorage.setItem('voucherDatabase', JSON.stringify(result));
        })

        // const test = {
        //     name: 'VOUCHERTEST',
        //     reduction: 0.8
        // }

        // const testVoucher = fetch(`${urlHeroku}/vouchers`,{
        //     method: 'POST',
        //     headers: {
        //         'Content-Type': 'application/x-www-form-urlencoded'
        //     },
        //     body: new URLSearchParams(test)
        // } )

        // testVoucher.then(res => res.json()).then(result => console.log(result));

    }

    render() {

        // Construction de la coquille de l'application avec le contexte, le routeur et l'affichage commun.
        return (
            <AppContext.Provider value={ this.state }>  {/* Contexte de départ de l'application basé sur le state */}
                <BrowserRouter>                         {/* https://reacttraining.com/react-router/web/api/BrowserRouter */}

                    {/* Affichage commun de l'application. */}
                    <header>
                        <NavBar />
                        <h1>Geek Shop</h1>
                    </header>

                    {/* Routing de l'application. */}
                    <Switch>                            {/* https://reacttraining.com/react-router/web/api/Switch */}
                        <Route exact path="/">          {/* https://reacttraining.com/react-router/web/api/Route */}
                        <Home database={this.state.productDatabase} />
                        </Route>
                        <Route exact path="/basket">
                            <Basket />
                        </Route>
                        <Route exact path="/product/:code">
                            <Product />
                        </Route>
                    </Switch>
                </BrowserRouter>
            </AppContext.Provider>
        );
    }
}