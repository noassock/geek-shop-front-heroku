import React from 'react';

import { ProductList } from '../components/ProductList';



// Feuille de styles spécifique à cette page (importation via Webpack).
import './home.css';


export function Home(props) {

    return (
        <main>
            <div className='bg-warning'>
                <h3 className='text-center text-success font-weight-bold'>Temps limité ! : -50% sur le panier avec le code NOEL2021</h3>
            </div>
            <h2>Accueil</h2>
            <ProductList database={props.database}/>
        </main>
    );
}