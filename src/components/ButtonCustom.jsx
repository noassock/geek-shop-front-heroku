import React from 'react';

class ButtonCustom extends React.Component {
    render() {
        return (
            <button onClick={this.props.onClick} disabled={this.props.disabled}>{this.props.children}</button>
        );
    };
};

export default ButtonCustom;