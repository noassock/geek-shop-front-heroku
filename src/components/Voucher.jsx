import React from 'react';
import { findVoucher } from '../lib/database';
import { formatVoucher } from '../lib/utilities';
import { AppContext } from '../AppContext';
import ButtonCustom from './ButtonCustom';

export class Voucher extends React.Component {

    static contextType = AppContext;

    state = {
        voucherCode : '',
        voucherValue : 0,
        voucherText : '',
        voucherTextColor : '',
        buttonDisabled : true
    }

    voucherChangeInput = (event) => {
        const voucherInput = event.target.value.toUpperCase();
        const voucher = findVoucher(voucherInput);
        if(!voucher){
            this.setState({buttonDisabled : true});
            this.setState({voucherValue : 0});
            this.setState({voucherText : `le code promo ${voucherInput} n'est pas/plus disponible`});
            this.setState({voucherTextColor : 'text-danger'});
        }
        else {
            this.setState({buttonDisabled : false});
            this.setState({voucherValue : voucher});
            this.setState({voucherText : `code promo ${voucher.name} valide ! ${formatVoucher(voucher.reduction)} de réduction !`});
            this.setState({voucherTextColor : 'text-success'});
        }
        this.setState({voucherCode : voucherInput});
    }

    voucherCheck = () => {
        this.setState({voucherCode : ''});
        this.setState({buttonDisabled : true});
        this.setState({voucherText : 'code promo appliqué :)'});
        this.setState({voucherTextColor : 'text-success'});

        this.context.setVoucherRate(this.state.voucherValue.reduction);
    }

    render() {
        
        if(this.context.basket.length === 0){
            return null;
        }

        return (
            <div className='voucher'>
                <input type="text" value={this.state.voucherCode} onChange={this.voucherChangeInput} />
                {/* <button onClick={this.voucherCheck}>Valider le bon de reduction</button> */}
                <ButtonCustom onClick={this.voucherCheck} disabled={this.state.buttonDisabled}>Valider le bon de reduction</ButtonCustom>
                <h3 className={this.state.voucherTextColor}>{this.state.voucherText}</h3>
            </div> 
        )
    }
}
